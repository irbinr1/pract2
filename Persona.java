package pract2;

public class Persona {
    private String nombre;
    private int edad;
    private String dni;
    private char sexo;
    private float peso;
    private float altura;

    public Persona() { 
        this("", 0, "", 'H', 0, 0);
    }

    public Persona(String nombre, int edad, String dni, char sexo, float peso, float altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.dni = dni;
        this.sexo = sexo;
        this.peso = peso;
        this.altura = altura;
    }

    public Persona(String nombre, int edad, char sexo) {
        this(nombre, edad, "", sexo, 0, 0);
    }
    
    public float calcularIMC() {
        final byte valor1 = 1;
        final byte valor2 = 0;
        final byte valor3 = -1;
        float imc = peso/(altura*altura);
        if (imc < 20) {
           return valor3;
        } else if(imc < 25){
           return valor2;
        }
        else {
            return valor1;
        }

    }
    
    
}
